Role Name
=========

Update OS

Requirements
------------

N/A

Role Variables
--------------

	update_type - full|minimal

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- name: Init OS configation
  hosts: all
  #strategy: debug
  gather_facts: yes
  become: yes
  roles:
    - role: ansible-os-init
    	update_type: full
      tags: [ 'all' ]
```

License
-------

BSD

Author Information
------------------

Jakub K. Boguslaw <jboguslaw@gmail.com> please inform me if you find any error/mistake
